#include "raylib.h"
#include <assert.h>
#include <math.h>

#define W 800
#define H 800


float field[H][W];
float threshold = 0.2f;
Image img;
Texture texture;


#define N_BALLS 2
Vector2 balls[N_BALLS];
Vector2 velocities[N_BALLS];


float falloff(float dist2) {
  // Parameters
  // ----------
  float sigma2 = 70.0f * 70.0f;
  float len2 = 400.0f * 400.0f;
  float power = 8.0f;

  // Calculation
  // -----------
  float main_part = expf(-dist2 / (sigma2)) + 0.05;
  float secondary_part = 0.05 * (1.0 - expf(-powf(dist2 / len2, power)));
  float ternary_part = 0.05f * sinf(0.1f * dist2);
  return main_part - secondary_part + ternary_part;
}


float falloff_simple(float dist2) {
  return 2000.0f / (1.0f + dist2);
}

float power_falloff(float dist, float power) {
  return 1000.0f * threshold / (1.0f + powf(dist, power));
}



void update_field(void) {
  float start_value = 2000.0f;
  // for (int b = 0; b < N_BALLS; ++b) {
    // balls[b].x += velocities[b].x;
    // balls[b].y += velocities[b].y;

    // if (balls[b].x > W) {
      // balls[b].x -= W;
    // }
    // if (balls[b].x < 0) {
      // balls[b].x += W;
    // }
    // if (balls[b].y > H) {
      // balls[b].y -= H;
    // }
    // if (balls[b].y < 0) {
      // balls[b].y += H;
    // }
  // }

  for (int b = 0; b < N_BALLS; ++b) {
    for (int j = 0; j < H; j++) {
      for (int i = 0; i < W; i++) {
        float dist2 = ((i - balls[b].x) * (i - balls[b].x) + (j - balls[b].y) * (j - balls[b].y));

        if (b == 0) {
          field[j][i] = falloff(dist2);
        } else {
          field[j][i] += falloff(dist2);
        }
      }
    }
  }




  // for (int j = 0; j < H; j++) {
    // for (int i = 0; i < W; i++) {
      // // float dist = sqrtf((i - ball1_x) * (i - ball1_x) + (j - ball1_y) * (j - ball1_y));
      // float dist = ((i - ball1_x) * (i - ball1_x) + (j - ball1_y) * (j - ball1_y));
      // // field[j][i] = start_value / (dist + 1);
      // // field[j][i] = power_falloff(dist, 1.0f);
      // field[j][i] = falloff(dist);
    // }
  // }

  // for (int j = 0; j < H; j++) {
    // for (int i = 0; i < W; i++) {
      // // float dist = sqrtf((i - ball2_x) * (i - ball2_x) + (j - ball2_y) * (j - ball2_y));
      // float dist = ((i - ball2_x) * (i - ball2_x) + (j - ball2_y) * (j - ball2_y));
      // // field[j][i] += start_value / (dist + 1);
      // field[j][i] += falloff(dist);
    // }
  // }
}


void update_image(void) {
  for (int j = 0; j < H; j++) {
    for (int i = 0; i < W; i++) {
      int index = j * W + i;
      // Color color = (field[j][i] > threshold) ? ColorFromHSV((int)(150 * field[j][i]) % 360, 0.8, 0.8) : BLACK;
      // Color color = (field[j][i] > threshold) ? RED : BLACK;
      Color color = (field[j][i] > threshold) ? RED : (Color){51, 51, 51, 255};
      ImageDrawPixel(&img, i, j, color);
    }
  }
}

void update_texture(void) {
  UpdateTexture(texture, img.data);
}


int main(void) {
  update_field();
  InitWindow(W, H, "Metaballs");
  SetTargetFPS(60);

  img = GenImageColor(W, H, BLACK);
  assert(img.format == PIXELFORMAT_UNCOMPRESSED_R8G8B8A8);
  texture = LoadTextureFromImage(img);

  update_image();
  update_texture();


  balls[0].x = 500.0f;
  balls[0].y = 500.0f;
  balls[1].x = 300.0f;
  balls[1].y = 300.0f;


  float ball_speed = 1000.0f / 60.0f;
  for (int b = 0; b < N_BALLS; ++b) {
    velocities[b].x = GetRandomValue(-ball_speed, ball_speed);
    velocities[b].y = GetRandomValue(-ball_speed, ball_speed);
  }
  while (!WindowShouldClose()) {
    BeginDrawing();
    ClearBackground(BLACK);
    DrawTexture(texture, 0, 0, WHITE);

    // DrawFPS(10, 10);
    EndDrawing();

    if (IsKeyReleased(KEY_SPACE)) {
      threshold += 100;
      update_image();
      update_texture();
    }


    if (IsKeyDown(KEY_W)) {
      ball1_y -= ball_speed;
    }
    if (IsKeyDown(KEY_A)) {
      ball1_x -= ball_speed;
    }
    if (IsKeyDown(KEY_S)) {
      ball1_y += ball_speed;
    }
    if (IsKeyDown(KEY_D)) {
      ball1_x += ball_speed;
    }
    update_field();
    update_image();
    update_texture();

    if (IsKeyReleased(KEY_P)) {
      TakeScreenshot("screenshot.png");
    }
  }


  UnloadTexture(texture);
  UnloadImage(img);
  CloseWindow();
}
