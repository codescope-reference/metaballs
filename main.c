#include "raylib.h"
#include <assert.h>
#include <math.h>

#define W 800
#define H 800

#define WG 200
#define HG 200
int spacing_x = W / (WG - 1);
int spacing_y = H / (HG - 1);

float field[HG][WG];
float threshold = 0.2f;
Image img;
Texture texture;

float ball1_x = 200.0f;
float ball1_y = 200.0f;
float ball2_x = 400.0f;
float ball2_y = 400.0f;

#define N_BALLS 3
Vector2 other_balls[N_BALLS];
void init_other_balls(void) {
  for (int i = 0; i < N_BALLS; ++i) {
    other_balls[i].x = GetRandomValue(0, W);
    other_balls[i].y = GetRandomValue(0, H);
  } 
}

float falloff(float dist2) {
  // Parameters
  // ----------
  float sigma2 = 70.0f * 70.0f;
  float len2 = 400.0f * 400.0f;
  float power = 8.0f;

  // Calculation
  // -----------
  float main_part = expf(-dist2 / (sigma2)) + 0.05;
  float secondary_part = 0.05 * (1.0 - expf(-powf(dist2 / len2, power)));
  float ternary_part = 0.05f * sinf(0.1f * dist2);
  return main_part - secondary_part + ternary_part / (1.0 + dist2 / 100000.0f);

}

float falloff_simple(float dist2) {
  return 2000.0f / (1.0f + dist2);
}



void update_field(void) {
  for (int j = 0; j < HG; ++j) {
    for (int i = 0; i < WG; ++i) {
      // Ball 1
      // ------
      float dist2 = (i * spacing_x - ball1_x) * (i * spacing_x - ball1_x) + (j * spacing_y - ball1_y) * (j * spacing_y - ball1_y);
      field[j][i] = falloff(dist2);

      // Ball 2
      // ------
      dist2 = (i * spacing_x - ball2_x) * (i * spacing_x - ball2_x) + (j * spacing_y - ball2_y) * (j * spacing_y - ball2_y);
      field[j][i] += falloff_simple(dist2);

      // Other balls
      // -----------
      for (int k = 0; k < N_BALLS; ++k) {
        dist2 = (i * spacing_x - other_balls[k].x) * (i * spacing_x - other_balls[k].x) + (j * spacing_y - other_balls[k].y) * (j * spacing_y - other_balls[k].y);
        field[j][i] += falloff_simple(dist2);
      }
    }
  }
}


void update_image(void) {
  for (int j = 0; j < H; j++) {
    for (int i = 0; i < W; i++) {
      int index = j * W + i;
      // Color color = (field[j][i] > threshold) ? ColorFromHSV((int)(150 * field[j][i]) % 360, 0.8, 0.8) : BLACK;
      // Color color = (field[j][i] > threshold) ? RED : BLACK;
      Color color = (field[j][i] > threshold) ? RED : (Color){51, 51, 51, 255};
      ImageDrawPixel(&img, i, j, color);
    }
  }
}


void update_texture(void) {
  UpdateTexture(texture, img.data);
}


void marching_squares_int(void) {
    for (int j = 0; j < HG - 1; ++j) {
    for (int i = 0; i < WG - 1; ++i) {
      int state = 0;

      // Lower left
      state += (int)(field[j + 1] [i    ] > threshold) << 0;
      // Lower right
      state += (int)(field[j + 1] [i + 1] > threshold) << 1;
      // Upper right
      state += (int)(field[j    ] [i + 1] > threshold) << 2;
      // Upper left
      state += (int)(field[j    ] [i    ] > threshold) << 3;


      int upper_left_x  = i       * spacing_x;
      int upper_left_y  = j       * spacing_y;
      int lower_left_x  = i       * spacing_x;
      int lower_left_y  = (j + 1) * spacing_y;
      int lower_right_x = (i + 1) * spacing_x;
      int lower_right_y = (j + 1) * spacing_y;
      int upper_right_x = (i + 1) * spacing_x;
      int upper_right_y = j       * spacing_y;

      switch (state) {
        case 0:
          continue;
        case 1: {
          int x1 = (upper_left_x + lower_left_x) / 2;
          int y1 = (upper_left_y + lower_left_y) / 2;
          int x2 = (lower_left_x + lower_right_x) / 2;
          int y2 = (lower_left_y + lower_right_y) / 2;

          DrawLine(x1, y1, x2, y2, RED);
          break;
        }
        case 2: {
          int x1 = (lower_right_x + upper_right_x) / 2;
          int y1 = (lower_right_y + upper_right_y) / 2;
          int x2 = (lower_left_x + lower_right_x) / 2;
          int y2 = (lower_left_y + lower_right_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);
          break;
        }
        case 3: {
          int x1 = (upper_left_x + lower_left_x) / 2;
          int y1 = (upper_left_y + lower_left_y) / 2;
          int x2 = (lower_right_x + upper_right_x) / 2;
          int y2 = (lower_right_y + upper_right_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);
          break;
        }
        case 4: {
          int x1 = (upper_right_x + upper_left_x) / 2;
          int y1 = (upper_right_y + upper_left_y) / 2;
          int x2 = (upper_right_x + lower_right_x) / 2;
          int y2 = (upper_right_y + lower_right_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);
          break;
        }
        case 5: {
          int x1 = (upper_right_x + upper_left_x) / 2;
          int y1 = (upper_right_y + upper_left_y) / 2;
          int x2 = (upper_left_x + lower_left_x) / 2;
          int y2 = (upper_left_y + lower_left_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);

          x1 = (upper_right_x + lower_right_x) / 2;
          y1 = (upper_right_y + lower_right_y) / 2;
          x2 = (lower_left_x + lower_right_x) / 2;
          y2 = (lower_left_y + lower_right_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);
          break;
        }
        case 6: {
          int x1 = (upper_right_x + upper_left_x) / 2;
          int y1 = (upper_right_y + upper_left_y) / 2;
          int x2 = (lower_left_x + lower_right_x) / 2;
          int y2 = (lower_left_y + lower_right_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);
          break;
        }
        case 7: {
          int x1 = (upper_right_x + upper_left_x) / 2;
          int y1 = (upper_right_y + upper_left_y) / 2;
          int x2 = (lower_left_x + upper_left_x) / 2;
          int y2 = (lower_left_y + upper_left_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);
          break;
        }
        case 8: {
          int x1 = (upper_right_x + upper_left_x) / 2;
          int y1 = (upper_right_y + upper_left_y) / 2;
          int x2 = (upper_left_x + lower_left_x) / 2;
          int y2 = (upper_left_y + lower_left_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);
          break;
        }
        case 9: {
          int x1 = (upper_right_x + upper_left_x) / 2;
          int y1 = (upper_right_y + upper_left_y) / 2;
          int x2 = (lower_left_x + lower_right_x) / 2;
          int y2 = (lower_left_y + lower_right_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);
          break;
        }
        case 10: {
          int x1 = (upper_left_x + lower_left_x) / 2;
          int y1 = (upper_left_y + lower_left_y) / 2;
          int x2 = (lower_left_x + lower_right_x) / 2;
          int y2 = (lower_left_y + lower_right_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);

          x1 = (upper_left_x + upper_right_x) / 2;
          y1 = (upper_left_y + upper_right_y) / 2;
          x2 = (upper_right_x + lower_right_x) / 2;
          y2 = (upper_right_y + lower_right_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);
          break;
        }
        case 11: {
          int x1 = (upper_left_x + upper_right_x) / 2;
          int y1 = (upper_left_y + upper_right_y) / 2;
          int x2 = (upper_right_x + lower_right_x) / 2;
          int y2 = (upper_right_y + lower_right_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);
          break;
        }
        case 12: {
          int x1 = (upper_left_x + lower_left_x) / 2;
          int y1 = (upper_left_y + lower_left_y) / 2;
          int x2 = (upper_right_x + lower_right_x) / 2;
          int y2 = (upper_right_y + lower_right_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);
          break;
        }
        case 13: {
          int x1 = (lower_left_x + lower_right_x) / 2;
          int y1 = (lower_left_y + lower_right_y) / 2;
          int x2 = (upper_right_x + lower_right_x) / 2;
          int y2 = (upper_right_y + lower_right_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);
          break;
        }
        case 14: {
          int x1 = (upper_left_x + lower_left_x) / 2;
          int y1 = (upper_left_y + lower_left_y) / 2;
          int x2 = (lower_left_x + lower_right_x) / 2;
          int y2 = (lower_left_y + lower_right_y) / 2;
          DrawLine(x1, y1, x2, y2, RED);
          break;
        }
        case 15: {
          continue;
        }
      }
    }
  }
}


#define UPPER_LEFT(num) int idx_x##num = i; int idx_y##num = j;
#define UPPER_RIGHT(num) int idx_x##num = i + 1; int idx_y##num = j;
#define LOWER_LEFT(num) int idx_x##num = i; int idx_y##num = j + 1;
#define LOWER_RIGHT(num) int idx_x##num = i + 1; int idx_y##num = j + 1;

#define MS_LINE(corner1, corner2, corner3, corner4) \
 {                                                  \
  float xm1, ym1, xm2, ym2;                         \
  {                                                 \
    corner1(1)                                      \
    corner2(2)                                      \
    float z1 = field[idx_y1][idx_x1];               \
    float x1 = idx_x1 * spacing_x;                  \
    float y1 = idx_y1 * spacing_y;                  \
    float z2 = field[idx_y2][idx_x2];               \
    float x2 = idx_x2 * spacing_x;                  \
    float y2 = idx_y2 * spacing_y;                  \
                                                    \
    float t1 = (threshold - z1) / (z2 - z1);        \
    xm1 = x1 + t1 * (x2 - x1);                      \
    ym1 = y1 + t1 * (y2 - y1);                      \
  }                                                 \
  {                                                 \
    corner3(1)                                      \
    corner4(2)                                      \
    float z1 = field[idx_y1][idx_x1];               \
    float x1 = idx_x1 * spacing_x;                  \
    float y1 = idx_y1 * spacing_y;                  \
    float z2 = field[idx_y2][idx_x2];               \
    float x2 = idx_x2 * spacing_x;                  \
    float y2 = idx_y2 * spacing_y;                  \
                                                    \
    float t1 = (threshold - z1) / (z2 - z1);        \
    xm2 = x1 + t1 * (x2 - x1);                      \
    ym2 = y1 + t1 * (y2 - y1);                      \
  }                                                 \
                                                    \
  DrawLineEx((Vector2) {xm1, ym1}, (Vector2){xm2, ym2}, 2.0f, RED); \
  /*DrawLine(xm1, ym1, xm2, ym2, RED);                */\
 }                                                  \


void marching_squares(void) {
  for (int j = 0; j < HG - 1; ++j) {
    for (int i = 0; i < WG - 1; ++i) {
      int state = 0;

      // Lower left
      state += (int)(field[j + 1] [i    ] > threshold) << 0;
      // Lower right
      state += (int)(field[j + 1] [i + 1] > threshold) << 1;
      // Upper right
      state += (int)(field[j    ] [i + 1] > threshold) << 2;
      // Upper left
      state += (int)(field[j    ] [i    ] > threshold) << 3;

      switch (state) {
        case 0:
          continue;
        case 1: {
          MS_LINE(UPPER_LEFT, LOWER_LEFT, LOWER_LEFT, LOWER_RIGHT);
          break;
        }
        case 2: {
          MS_LINE(UPPER_RIGHT, LOWER_RIGHT, LOWER_RIGHT, LOWER_LEFT);
          break;
        }
        case 3: {
          MS_LINE(UPPER_LEFT, LOWER_LEFT, UPPER_RIGHT, LOWER_RIGHT);
          break;
        }
        case 4: {
          MS_LINE(UPPER_LEFT, UPPER_RIGHT, UPPER_RIGHT, LOWER_RIGHT);
          break;
        }
        case 5: {
          MS_LINE(UPPER_LEFT, LOWER_LEFT, UPPER_LEFT, UPPER_RIGHT);
          MS_LINE(UPPER_RIGHT, LOWER_RIGHT, LOWER_RIGHT, LOWER_LEFT);
          break;
        }
        case 6: {
          MS_LINE(UPPER_LEFT, UPPER_RIGHT, LOWER_RIGHT, LOWER_LEFT);
          break;
        }
        case 7: {
          MS_LINE(UPPER_LEFT, UPPER_RIGHT, LOWER_LEFT, UPPER_LEFT);
          break;
        }
        case 8: {
          MS_LINE(UPPER_LEFT, UPPER_RIGHT, UPPER_LEFT, LOWER_LEFT);
          break;
        }
        case 9: {
          MS_LINE(UPPER_LEFT, UPPER_RIGHT, LOWER_LEFT, LOWER_RIGHT);
          break;
        }
        case 10: {
          MS_LINE(UPPER_LEFT, UPPER_RIGHT, UPPER_RIGHT, LOWER_RIGHT);
          MS_LINE(UPPER_LEFT, LOWER_LEFT, LOWER_LEFT, LOWER_RIGHT);
          break;
        }
        case 11: {
          MS_LINE(UPPER_LEFT, UPPER_RIGHT, UPPER_RIGHT, LOWER_RIGHT);
          break;
        }
        case 12: {
          MS_LINE(UPPER_LEFT, LOWER_LEFT, UPPER_RIGHT, LOWER_RIGHT);
          break;
        }
        case 13: {
          MS_LINE(UPPER_RIGHT, LOWER_RIGHT, LOWER_RIGHT, LOWER_LEFT);
          break;
        }
        case 14: {
          MS_LINE(UPPER_LEFT, LOWER_LEFT, LOWER_LEFT, LOWER_RIGHT);
          break;
        }
        case 15: {
          // DrawRectangle(i * spacing_x, j * spacing_y, spacing_x, spacing_y, RED);
          continue;
        }
      }
    }
  }
}


int main(void) {
  update_field();
  SetRandomSeed(123);
  init_other_balls();
  InitWindow(W, H, "Metaballs + Marching Squares");
  SetTargetFPS(60);

  img = GenImageColor(W, H, BLACK);
  assert(img.format == PIXELFORMAT_UNCOMPRESSED_R8G8B8A8);
  texture = LoadTextureFromImage(img);

  // update_image();
  // update_texture();

  float ball_speed = 500.0f / 60.0f;
  bool use_interpolation = true;
  while (!WindowShouldClose()) {
    BeginDrawing();
    ClearBackground((Color){51, 51, 51, 255});

    if (use_interpolation) {
      marching_squares();
    } else {
      marching_squares_int();
    }


    DrawFPS(10, 10);
    EndDrawing();

    if (IsKeyReleased(KEY_I)) {
      use_interpolation = !use_interpolation;
    }


    if (IsKeyDown(KEY_W)) {
      ball1_y -= ball_speed;
    }
    if (IsKeyDown(KEY_A)) {
      ball1_x -= ball_speed;
    }
    if (IsKeyDown(KEY_S)) {
      ball1_y += ball_speed;
    }
    if (IsKeyDown(KEY_D)) {
      ball1_x += ball_speed;
    }
    update_field();

    if (IsKeyReleased(KEY_P)) {
      TakeScreenshot("screenshot.png");
    }
  }


  UnloadTexture(texture);
  UnloadImage(img);
  CloseWindow();
}
